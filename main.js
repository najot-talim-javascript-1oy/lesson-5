// Func1. a sonning ixtiyoriy n-darajasini hisoblovchi power(a, n) nomli funksiya hosil qiling. QY power(3, 5) => 243

// function power(a, n) {
//     let result = 1;

//     for (let i = 0; i < n; i++) {
//         result  *= a;
//     }

//     return result;
// }
// console.log(power(3, 5));


// Func2. 2 ta a va b sonning o'rta arifmetigi (a + b) / 2 va geometrigi (a + b) ^ (1 / 2) hisoblovchi mean(a, b) nomli funksiya hosil qiling. QYM mean(12, 48) => 30, 24

// function mean(a, b) {
//     let arithmetic = (a + b) / 2;
//     let geometric = Math.sqrt(a * b);
//     console.log(arithmetic, geometric);
// }
// console.log(mean(12, 48));



// Func3. Haqiqiy sonning ishorasini aniqlovchi sign(n) nomli funksiya hosil qiling. Funksiya argumenti noldan kichik bo'lsa -1; noldan katta bo'lsa 1; nolga teng bo'lsa 0 qiymat qaytarsin. QY sign(10) => 1

// function sign(n) {
//     if(n >= 0){
//         return 1
//     }else if (n < 0) {
//         return -1;
//     } else {
//         return 0;
//     }
// }
// console.log(sign(10));


// Func4. A*x^2 + B*x + C = 0 ko'rinishidagi tenglama kvadrat tenglama deyiladi. (A noldan farqli son). Kvadrat tenglamaning ildizlar sonini aniqlovchi numberOfRoots(A, B, C) nomli funksiya hosil qiling. D = B^2 - 4*A*C. Agar D > 0 bo’lsa 2 ta, D = 0 bo’lsa 1 ta, D < 0 bo’lsa 0 ta. QY numberOfRoots (1, -6, 9) => 1


// function numberOfRoots(A, B, C) {
//     let D = B * B - 4 * A * C; 
//     if (D > 0) {
//       return 2;
//     } else if (D === 0) {
//       return 1;
//     } else {
//       return 0;
//     }
// }
// console.log(numberOfRoots(1, -6, 9));


// Func5. Doiraning yuzini hisoblovchi areaCircle(R) nomli funksiya hosil qiling. Doiraning yuzi S = π*R^2 orqali hisoblanadi. QY

// function areaCircle(R) {
//     const π = 3.14159;
//     let S = π * R * R;
//     return S
// }
// console.log(areaCircle(6));



// Func6. A va B sonlari orasidagi sonlar yig'indisini hisoblovchi sumRange(A, B) nomli funksiya hosil qiling. Agar A > B bo'lsa, funksiya 0 qiymat qaytaradi. QY sumRange(8, 10) => 27

// function sumRange(A, B)  {
//     if (A > B){
//         return 0;
//     }else{
//         let summ = 0;
//         for (let i = A; i <= B; i++) {
//             summ += i;
//         }
//         return summ;
//     }
// }
// console.log(sumRange(8, 10));


// Func7. Arifmetik amallarni bajaruvchi calc(A, B, S) funksiyasini hosil qiling. A va B haqiqiy sonlar. S o'zgaruchisi orqali bajariladigan arifmetik amal aniqlanadi. “-” – ayirish, “*” – ko'paytirish, “/” - bo'lish, “+” - qo'shish va boshqa belgilar uchun 0 qaytarsin. QY calc(10, 15, “*”) => 150 calc(7, 8, “+”) => 15

// function calc(A, B, C) {
//     if(C === '+'){
//         return A + B;
//     }else if(C === "-"){
//         return A - B;
//     }else if(C === "*"){
//         return A * B;
//     }else if(C === "/"){
//         return A / B;
//     }else{
//         return 0;
//     }
// }
// console.log(calc(10, 15, "+"));
// console.log(calc(10, 15, "-"));
// console.log(calc(10, 15, "*"));



// Func8. Butun sonning juft - toqligini aniqlovchi isEven(K) funksiyasini hosil qiling. Funksiya K juft son bo'lsa - true, aks xolda false qiymat qaytarsin. QY isEven(10) => true

// function isEven(K) {
//     if(K % 2 === 0){
//         return true;
//     }else{
//         return false;
//     }
// }
// console.log(isEven(10));


// Func9. Kiritilgan 3 ta a, b, c sonlarning eng kichigini, o’rtachasini va eng kattasini chiqaruvchi sortABC(a, b, c) nomli dastur tuzing. QYM sortABC(10, 5, 8) => 5, 8, 10

// function sortABC(a, b, c) {
//     let kichik = Math.min(a, b, c);
//     let katta = Math.max(a, b, c);
//     let ortas = (a + b + c) - kichik - katta;
//     console.log(kichik + ", " + ortas + ", " + katta);
// }
// console.log(sortABC(10, 5, 8));


// Func10. isPowerN(K, N) mantiqiy funksiyasini hosil qiling. (K > 0). Agar K soni N soninig biror darajasi bo'lsa - true, aks xolda false qiymat qaytarilsin. QY

// function isPowerN(K, N) {
//     if (K == 1) {
//         return true
//     }
//     let power = N;
//     while(power <= K){
//         if(power == K){
//             return true;
//         }
//         power *= N;
//     }
//     return false
// }
// console.log(isPowerN(2,2));

// Func11. isPrime(N) mantiqiy funksiyasini hosil qiling. (N > 0). Agar N soni tub bo'lsa - true, aks holda false qiymat qaytarilsin. QY isPrime(7)  => true

// function isPrime(num) {
//     if (num <= 1) {
//       return false;
//     }
//     for (let i = 2; i <= Math.sqrt(num); i++) {
//       if (num % i === 0) {
//         return false;
//       }
//     }
//     return true;
// }
// function numberOfPrime(n) {
//     let count = 0;
//     for (let i = 2; i <= n; i++) {
//       if (isPrime(i)) {
//         count++;
//       }
//     }
//     return count;
// }
  
// console.log(numberOfPrime(10));



// Func13. Butun qiymat qaytaruvchi digitNth(K, N) funksiyasini hosil qiling. (K > 0). Funksiya K sonining N-raqamini qaytarsin. Agar K soni raqamlari N dan kichik bo'lsa, -1 qaytarilsin. digitCount funksiyasidan foydalaning.QY digitNth(105782, 5) => 8 digitNth(1057, 5) => -1

// function digitCount(num) {
//     let count = 0;
//     while (num > 0) {
//     count++;
//     num = Math.floor(num / 10);
//     }
//     return count;
// }
// function digitNth(K, N) {
//     let count = digitCount(K);
//     if (N > count) {
//     return -1;
//     }
//     for (let i = 1; i <= count; i++) {
//     let digit = K % 10;
//     if (i === N) {
//     return digit;
//     }
//     K = Math.floor(K / 10);
//     }
// }
    
// console.log(digitNth(1245456, 5)); 
// console.log(digitNth(9234, 5));


// Func14. N sonining raqamlaridan teskari tartibda hosil bo’ladigan sonni qaytaruvchi inverseNumber(N) nomli funksiya hosil qiling. QY

// function inverseNumber(N) {
//     let inverse = 0;
//     while(N > 0)   {
//         inverse = inverse * 10 + (N % 10);
//         N = Math.floor(N / 10);
//     }
//     return inverse;
// }
// console.log(inverseNumber(56814));



//Func15. isPalindrom(N) mantiqiy funksiyasini hosil qiling. (N > 0). Agar N soni palindrom bo'lsa - true, aks holda false qiymat qaytarilsin. inverseNumber funksiyasidan foydalaning. Palindromik son - chapda ham, o’ngdan ham o’qilganda bir xil bo’ladigan son. Masalan, 123321, 78987. QY isPalindrom(1678761) => true

/*function isPalindrom(N) {
    let temp = N;
    let reversedNumber = 0;
    
    while (temp > 0) {
    let digit = temp % 10;
    reversedNumber = reversedNumber * 10 + digit;
    temp = Math.floor(temp / 10);
    }

    if (reversedNumber === Number(N)) {
    return true
    } else {
    return false
    }
}
console.log(isPalindrom(1678761));
*/

// function inverseNumber(N) {
//     let temp = 0;
//     while (N > 0) {
//       temp = result * 10 + N % 10;
//       N = Math.floor(N / 10);
//     }
//     return temp;
// } 
// function isPalindrom(N) {
//     return N === inverseNumber(N);
// }  
// console.log(isPalindrom(1678761)); 



// Func16. 1 dan N ga sonlar ko’paytmasini qaytaruvchi factorial(N) nomli funksiya hosil qiling. Agar N manfiy bo’lib qolsa, 1 qaytarilsin. QY

// function factorial(N) {
//     if (N < 0) {
//       return 1;
//     } else if (N == 0) {
//       return 1;
//     } else {
//       return N * factorial(N - 1);
//     }
// }
// console.log(factorial(-5));


// Func17. 1 dan N bo’lgan sonlar ichida 3 bo’linadigan sonlar yig’indisini hisoblovchi getSum3(N) nomli dastur yozing. getSum3(15) => 45

// function getSum3(N) {
//     let sum = 0;
//     for (let i = 1; i <= N; i++) {
//       if (i % 3 === 0) {
//         sum += i;
//       }
//     }
//     return sum;
// }
// console.log(getSum3(15));



// Func18. 1 dan N ga sonlar bo’lgan juft va toqlar sonlar yig’indisini qaytaruvchi sumOddEven(N) nomli funksiya hosil qiling. QYM sumOddEven(10) => 30, 25

// function sumOddEven(N)  {
//     let sumJuft = 0;
//     let sumToq = 0;
//     for (let i = 1; i <= N; i++) {
//         if (i % 2 === 0) {
//             sumJuft += i;
//         }else{
//             sumToq += i;
//         }
//     }
//     return sumJuft + ', ' + sumToq;
// }
// console.log(sumOddEven(10));


//Func19. invertTime(H, M, S) funksiyasini hosil qiling. H - soat, M - minut, S - sekund. Funksiya orqali berilgan soat, minut va sekundni T sekundga almashtiruvchi programma tuzilsin. invertTime(0, 6, 40) => 400

// function invertTime(H, M, S) {
//     let T = H * 3600 + M * 60 + S;
//     return T;
// }
// console.log(invertTime(0, 6, 40));


// Func20. decTime(H, M, S) funksiyasini hosil qiling. H - soat, M - minut, S - sekund. Funksiya berilgan vaqtdan 1 sekund oldingi vaqtni ko’rsatsin. decTime(0, 6, 40) => 00:06:39

// function decTime(H, M, S) {
//     let totalSec = H * 3600 + M * 60 + S;
//     totalSec -= 1;
//     let hou = Math.floor(totalSec / 3600).toString().padStart(2, '0');
//     let minu = Math.floor((totalSec % 3600) / 60).toString().padStart(2, '0');
//     let secon = (totalSec % 60).toString().padStart(2, '0');
//     return `${hou}:${minu}:${secon}`;
// }
// console.log(decTime(0, 6, 40)); 


// Func21. Mantiqiy qiymat qaytaruvchi isLeapYear(Y) funksiyasini hosil qiling. Funksiya berilgan Y - yil kabisa yili bo'lsa true, aks holda false qiymat qaytarsin.

// function isLeapYear(Y) {
//     if (Y % 4 !== 0) {
//       return false;
//     } else if (Y % 100 !== 0) {
//       return true;
//     } else if (Y % 400 !== 0) {
//       return false;
//     } else {
//       return true;
//     }
// }
// console.log(isLeapYear(1200));


//  Func22. isLeapYear(Y) funksiyasidan foydalangan xolda, butun qiymat qaytaruvchi monthDays(M, Y) funksiyasini hosil qiling. Funksiya berilgan Y - yilning M - oyi kunlar sonini qaytarsin. monthDays(2, 2020) => 28

// function isLeapYear(Y) {
//     return (Y % 4 === 0) && (Y % 100 !== 0 || Y % 400 === 0);
// }  
// function monthDays(M, Y) {
//     switch (M) {
//       case 2:
//         return isLeapYear(Y) ? 28 : 28;
//       case 4:
//       case 6:
//       case 9:
//       case 11:
//         return 30;
//       default:
//         return 31;
//     }
// }
// console.log(monthDays(3, 2021));


// Func23. monthDays funksiyasidan foydalangan xolda, prevDate (D, M, Y) funksiyasini hosil qiling. Funksiya berilgan sanadan oldingi sanani aniqlasin, D - kun, Y - yil, M - oyini qaytarsin. QY prevDate (10, 3, 2022) => 09.03.2022

// function prevDate(D, M, Y) {
//     if (D > 1) {
//       return (D - 1) + '.' + M + '.' + Y;
//     } else {
//       var pMonth = M - 1;
//       var pYear = Y;
//       if (pMonth === 0) {
//         pMonth = 12;
//         pYear--;
//       }
//       var days = monthDays(pMonth, pYear);
//       return days + '.' + pMonth + '.' + pYear;
//     }
// }
// console.log(prevDate(10, 3, 2022));


// Func24. monthDays funksiyasidan foydalangan xolda, nextDate(D, M, Y) funksiyasini hosil qiling. Funksiya berilgan sanadan keying sanani aniqlasin, D-kun, Y yil, M - oyini qaytarsin. QY nextDate (10, 3, 2022) => 11.03.2022

// function findSunOrientation(date) {
//     const latitude = 51.5074; 
//     const longitude = 0.1278;
  
//     const year = date.getFullYear();
//     const month = date.getMonth() + 1;
//     const day = date.getDate();
  
//     const n = Math.floor((275 * month) / 9) - 2 * Math.floor((month + 9) / 12) + day - 30;
//     const L = 4.876 + 0.017 * (year + (n / 365)) + longitude / 360;
//     const G = 357.528 + 0.98560028 * (n + 10) + 1.914 * Math.sin((Math.PI / 180) * L) + 0.02 * Math.sin(2 * (Math.PI / 180) * L);
//     const dec = 23.439 * Math.sin((Math.PI / 180) * (0.967 * G));
//     const h = Math.acos(Math.cos((Math.PI / 180) * (90.833)) / (Math.cos((Math.PI / 180) * latitude) * Math.cos((Math.PI / 180) * dec)) - Math.tan((Math.PI / 180) * latitude) * Math.tan((Math.PI / 180) * dec));
//     const sunrise = 720 - (4 * (longitude + (180 / Math.PI) * h)) - (0.0066011 * (720 - (4 * (longitude + (180 / Math.PI) * h)))) / 60;
  
//     if (sunrise > 180) {
//       return "G’arb";
//     } else {
//       return "Sharq";
//     }
// }



// Func25. N sonining bo’luvchilari soni va bo’luvchilari yi’gindisini chiqaruvchi getDividersNumberAndSum(N) nomli funksiya yozing. QYM getDividersNumberAndSum(12) => 6, 24

// function getDividersNumberAndSum(N) {
//     let count = 0;
//     let sum = 0;
//     for (let i = 1; i <= N; i++) {
//       if (N % i == 0) {
//         count++;
//         sum += i;
//       }
//     }
//     return count + ", " + sum;
// }
// console.log(getDividersNumberAndSum(12));